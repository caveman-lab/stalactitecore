﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CavemanLab.StalactiteProject.Core
{
    public abstract class BlockRecordBase
    {
        public virtual uint Version { get; set; }
        public virtual ulong Timestamp { get; set; }
        public abstract bool Validate();
    }
}
