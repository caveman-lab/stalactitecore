﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CavemanLab.StalactiteProject.Core
{
    public abstract class BlockBase
    {
        public virtual ulong Size { get; set; }
        public virtual BlockHeaderBase Header { get; set; }
        public virtual List<BlockRecordBase> Records { get; set; }
        public abstract bool Validate();
    }
}
