﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CavemanLab.StalactiteProject.Core
{
    public abstract class ChainBase
    {
        public virtual string Name { get; set; }
        public virtual uint Version { get; set; }
        public virtual uint Height { get; set; }
        public virtual ChainedBlockBase Genesis
        {
            get
            {
                return GetBlock(0);
            }
        }
        public virtual ChainedBlockBase Tip
        {
            get
            {
                return GetBlock(Height);
            }
        }
        public abstract ChainedBlockBase GetBlock(UInt256 id);
        public abstract ChainedBlockBase GetBlock(uint height);
        public bool Contains(UInt256 hash)
        {
            return GetBlock(hash) != null;
        }
    }
}
