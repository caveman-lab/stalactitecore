﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CavemanLab.StalactiteProject.Core
{
    public abstract class NetworkBase
    {
        public virtual string Name { get; set; }
        public virtual NetworkType Type { get; set; }
        public virtual uint Magic { get; set; }
        public virtual ChainBase Chain { get; set; }
    }
}
