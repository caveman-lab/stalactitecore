﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CavemanLab.StalactiteProject.Core
{
    public abstract class ChainedBlockBase
    {
        public virtual BlockBase Block { get; set; }
        public virtual uint Height { get; set; }
        public virtual ChainedBlockBase Prev { get; set; }
    }
}
