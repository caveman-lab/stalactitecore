﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CavemanLab.StalactiteProject.Core
{
    public abstract class BlockHeaderBase
    {
        public virtual uint Version { get; set; }
        public virtual ulong Timestamp { get; set; }
        public virtual UInt256 MerkleRootHash { get; set; }
        public virtual UInt256 PrevBlockHash { get; set; }
    }
}
