﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CavemanLab.StalactiteProject.Core
{
    public enum NetworkType
    {
        Mainnet,
        Testnet,
        Regtest
    }
}