﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using System.Diagnostics;

namespace CavemanLab.StalactiteProject.Core
{
    public static class Utils
    {
        public static byte[] ToBytes(ulong value, bool littleEndian)
        {
            if (littleEndian)
            {
                return new byte[]
                {
                    (byte)value,
                    (byte)(value >> 8),
                    (byte)(value >> 16),
                    (byte)(value >> 24),
                    (byte)(value >> 32),
                    (byte)(value >> 40),
                    (byte)(value >> 48),
                    (byte)(value >> 56),
                };
            }
            else
            {
                return new byte[]
                {
                    (byte)(value >> 56),
                    (byte)(value >> 48),
                    (byte)(value >> 40),
                    (byte)(value >> 32),
                    (byte)(value >> 24),
                    (byte)(value >> 16),
                    (byte)(value >> 8),
                    (byte)value,
                };
            }
        }

        public static uint ToUInt32(byte[] value, int index, bool littleEndian)
        {
            if (littleEndian)
            {
                return value[index]
                       + ((uint)value[index + 1] << 8)
                       + ((uint)value[index + 2] << 16)
                       + ((uint)value[index + 3] << 24);
            }
            else
            {
                return value[index + 3]
                       + ((uint)value[index + 2] << 8)
                       + ((uint)value[index + 1] << 16)
                       + ((uint)value[index + 0] << 24);
            }
        }

        public static bool ArrayEqual(byte[] a, byte[] b)
        {
            if (a == null && b == null)
                return true;
            if (a == null)
                return false;
            if (b == null)
                return false;
            return ArrayEqual(a, 0, b, 0, Math.Max(a.Length, b.Length));
        }
        public static bool ArrayEqual(byte[] a, int startA, byte[] b, int startB, int length)
        {
            if (a == null && b == null)
                return true;
            if (a == null)
                return false;
            if (b == null)
                return false;
            var alen = a.Length - startA;
            var blen = b.Length - startB;

            if (alen < length || blen < length)
                return false;

            for (int ai = startA, bi = startB; ai < startA + length; ai++, bi++)
            {
                if (a[ai] != b[bi])
                    return false;
            }
            return true;
        }
    }

    public static class ByteArrayExtensions
    {
        public static bool StartWith(this byte[] data, byte[] versionBytes)
        {
            if (data.Length < versionBytes.Length)
                return false;
            for (int i = 0; i < versionBytes.Length; i++)
            {
                if (data[i] != versionBytes[i])
                    return false;
            }
            return true;
        }

        public static byte[] SafeSubarray(this byte[] array, int offset, int count)
        {
            if (array == null)
                throw new ArgumentNullException(nameof(array));
            if (offset < 0 || offset > array.Length)
                throw new ArgumentOutOfRangeException("offset");
            if (count < 0 || offset + count > array.Length)
                throw new ArgumentOutOfRangeException("count");
            if (offset == 0 && array.Length == count)
                return array;
            var data = new byte[count];
            Buffer.BlockCopy(array, offset, data, 0, count);
            return data;
        }

        public static byte[] SafeSubarray(this byte[] array, int offset)
        {
            if (array == null)
                throw new ArgumentNullException(nameof(array));
            if (offset < 0 || offset > array.Length)
                throw new ArgumentOutOfRangeException("offset");

            var count = array.Length - offset;
            var data = new byte[count];
            Buffer.BlockCopy(array, offset, data, 0, count);
            return data;
        }

        public static byte[] Concat(this byte[] arr, params byte[][] arrs)
        {
            var len = arr.Length + arrs.Sum(a => a.Length);
            var ret = new byte[len];
            Buffer.BlockCopy(arr, 0, ret, 0, arr.Length);
            var pos = arr.Length;
            foreach (var a in arrs)
            {
                Buffer.BlockCopy(a, 0, ret, pos, a.Length);
                pos += a.Length;
            }
            return ret;
        }
    }
}