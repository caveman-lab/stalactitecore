﻿using Org.BouncyCastle.Asn1;
using Org.BouncyCastle.Math;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace CavemanLab.StalactiteProject.Core
{
    public class ECDSASignature
    {
        private readonly BigInteger _R;
        public BigInteger R
        {
            get
            {
                return this._R;
            }
        }
        private BigInteger _S;
        public BigInteger S
        {
            get
            {
                return this._S;
            }
        }
        public ECDSASignature(BigInteger r, BigInteger s)
        {
            this._R = r;
            this._S = s;
        }

        public ECDSASignature(BigInteger[] rs)
        {
            this._R = rs[0];
            this._S = rs[1];
        }

        public ECDSASignature(byte[] derSig)
        {
            try
            {
                var decoder = new Asn1InputStream(derSig);
                var seq = decoder.ReadObject() as DerSequence;
                if (seq == null || seq.Count != 2)
                    throw new FormatException(InvalidDERSignature);
                this._R = ((DerInteger)seq[0]).Value;
                this._S = ((DerInteger)seq[1]).Value;
            }
            catch (Exception ex)
            {
                throw new FormatException(InvalidDERSignature, ex);
            }
        }

        /**
        * What we get back from the signer are the two components of a signature, r and s. To get a flat byte stream
        * of the type used by Bitcoin we have to encode them using DER encoding, which is just a way to pack the two
        * components into a structure.
        */
        public byte[] ToDER()
        {
            // Usually 70-72 bytes.
            var bos = new MemoryStream(72);
            var seq = new DerSequenceGenerator(bos);
            seq.AddObject(new DerInteger(this.R));
            seq.AddObject(new DerInteger(this.S));
            seq.Close();
            return bos.ToArray();

        }

        private const string InvalidDERSignature = "Invalid DER signature";
        public static ECDSASignature FromDER(byte[] sig)
        {
            return new ECDSASignature(sig);
        }

        /// <summary>
        /// Enforce LowS on the signature
        /// </summary>
        public ECDSASignature MakeCanonical(BigInteger order)
        {
            if (!this.IsLowS(order))
            {
                return new ECDSASignature(this.R, order.Subtract(this.S));
            }
            else
                return this;
        }

        private bool IsLowS(BigInteger order)
        {
            return this.S.CompareTo(order.ShiftRight(1)) <= 0;
        }

        public static bool IsValidDER(byte[] bytes)
        {
            try
            {
                FromDER(bytes);
                return true;
            }
            catch (FormatException)
            {
                return false;
            }
            catch (Exception ex)
            {
                return false;
            }
        }
    }
}