﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;

namespace CavemanLab.StalactiteProject.Core
{
    //Encoding.ASCII is not portable
    public class ASCIIEncoder : DataEncoder
    {
        public override byte[] DecodeData(string encoded)
        {
            if (String.IsNullOrEmpty(encoded))
                return new byte[0];

            var r = new byte[encoded.Length];
            for (int i = 0; i < r.Length; i++)
            {
                r[i] = (byte)encoded[i];
            }

            return r.ToArray();
        }

        public override string EncodeData(byte[] data, int offset, int count)
        {
            return new String(data.Skip(offset).Take(count).Select(o => (char)o).ToArray()).Replace("\0", "");
        }
    }
}
