﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CavemanLab.StalactiteProject.Core.UnitTest
{
    [TestClass]
    public class Base32Test
    {
        [TestMethod]
        public void HelloWorld()
        {
            const string text = "Hello World!";
            const string base32Encoded = "JBSWY3DPEBLW64TMMQQQ====";
            byte[] textByteArray = Encoders.ASCII.DecodeData(text);

            Assert.IsTrue(Encoders.Base32.EncodeData(textByteArray).Equals(base32Encoded));
            Assert.IsTrue(Encoders.Base32.DecodeData(base32Encoded).SequenceEqual(textByteArray));
            Assert.IsTrue(Encoders.ASCII.EncodeData(Encoders.Base32.DecodeData(base32Encoded)).Equals(text));
        }

        [TestMethod]
        public void Cat()
        {
            const string text = "Cat";
            const string base32Encoded = "INQXI===";
            byte[] textByteArray = Encoders.ASCII.DecodeData(text);

            Assert.IsTrue(Encoders.Base32.EncodeData(textByteArray).Equals(base32Encoded));
            Assert.IsTrue(Encoders.Base32.DecodeData(base32Encoded).SequenceEqual(textByteArray));
            Assert.IsTrue(Encoders.ASCII.EncodeData(Encoders.Base32.DecodeData(base32Encoded)).Equals(text));
        }

        [TestMethod]
        public void Foo()
        {
            const string text = "Foo";
            const string base32Encoded = "IZXW6===";
            byte[] textByteArray = Encoders.ASCII.DecodeData(text);

            Assert.IsTrue(Encoders.Base32.EncodeData(textByteArray).Equals(base32Encoded));
            Assert.IsTrue(Encoders.Base32.DecodeData(base32Encoded).SequenceEqual(textByteArray));
            Assert.IsTrue(Encoders.ASCII.EncodeData(Encoders.Base32.DecodeData(base32Encoded)).Equals(text));
        }

        [TestMethod]
        public void Bar()
        {
            const string text = "Bar";
            const string base32Encoded = "IJQXE===";
            byte[] textByteArray = Encoders.ASCII.DecodeData(text);

            Assert.IsTrue(Encoders.Base32.EncodeData(textByteArray).Equals(base32Encoded));
            Assert.IsTrue(Encoders.Base32.DecodeData(base32Encoded).SequenceEqual(textByteArray));
            Assert.IsTrue(Encoders.ASCII.EncodeData(Encoders.Base32.DecodeData(base32Encoded)).Equals(text));
        }

        [TestMethod]
        public void LoremIpsum1()
        {
            const string text = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam auctor mollis odio, id pharetra enim auctor nec. Suspendisse imperdiet ultricies risus, quis tempus orci faucibus in. Maecenas dapibus arcu sed lacus sagittis pulvinar. Nam ut gravida diam. Cras rutrum sed nisl quis auctor. Vestibulum facilisis ex mauris, ut volutpat urna dictum ac. Donec quis enim rhoncus, sodales libero nec, pulvinar mi. Sed scelerisque interdum dui, at pulvinar neque faucibus a. Vivamus ac sollicitudin tellus. Sed ac vestibulum eros, ac semper quam. Phasellus vulputate id risus id scelerisque. Proin imperdiet et enim id venenatis. Phasellus pulvinar eu magna sit amet tempor.";
            const string base32Encoded = "JRXXEZLNEBUXA43VNUQGI33MN5ZCA43JOQQGC3LFOQWCAY3PNZZWKY3UMV2HK4RAMFSGS4DJONRWS3THEBSWY2LUFYQEK5DJMFWSAYLVMN2G64RANVXWY3DJOMQG6ZDJN4WCA2LEEBYGQYLSMV2HEYJAMVXGS3JAMF2WG5DPOIQG4ZLDFYQFG5LTOBSW4ZDJONZWKIDJNVYGK4TENFSXIIDVNR2HE2LDNFSXGIDSNFZXK4ZMEBYXK2LTEB2GK3LQOVZSA33SMNUSAZTBOVRWSYTVOMQGS3ROEBGWCZLDMVXGC4ZAMRQXA2LCOVZSAYLSMN2SA43FMQQGYYLDOVZSA43BM5UXI5DJOMQHA5LMOZUW4YLSFYQE4YLNEB2XIIDHOJQXM2LEMEQGI2LBNUXCAQ3SMFZSA4TVORZHK3JAONSWIIDONFZWYIDROVUXGIDBOVRXI33SFYQFMZLTORUWE5LMOVWSAZTBMNUWY2LTNFZSAZLYEBWWC5LSNFZSYIDVOQQHM33MOV2HAYLUEB2XE3TBEBSGSY3UOVWSAYLDFYQEI33OMVRSA4LVNFZSAZLONFWSA4TIN5XGG5LTFQQHG33EMFWGK4ZANRUWEZLSN4QG4ZLDFQQHA5LMOZUW4YLSEBWWSLRAKNSWIIDTMNSWYZLSNFZXC5LFEBUW45DFOJSHK3JAMR2WSLBAMF2CA4DVNR3GS3TBOIQG4ZLROVSSAZTBOVRWSYTVOMQGCLRAKZUXMYLNOVZSAYLDEBZW63DMNFRWS5DVMRUW4IDUMVWGY5LTFYQFGZLEEBQWGIDWMVZXI2LCOVWHK3JAMVZG64ZMEBQWGIDTMVWXAZLSEBYXKYLNFYQFA2DBONSWY3DVOMQHM5LMOB2XIYLUMUQGSZBAOJUXG5LTEBUWIIDTMNSWYZLSNFZXC5LFFYQFA4TPNFXCA2LNOBSXEZDJMV2CAZLUEBSW42LNEBUWIIDWMVXGK3TBORUXGLRAKBUGC43FNRWHK4ZAOB2WY5TJNZQXEIDFOUQG2YLHNZQSA43JOQQGC3LFOQQHIZLNOBXXELQ=";
            byte[] textByteArray = Encoders.ASCII.DecodeData(text);

            Assert.IsTrue(Encoders.Base32.EncodeData(textByteArray).Equals(base32Encoded));
            Assert.IsTrue(Encoders.Base32.DecodeData(base32Encoded).SequenceEqual(textByteArray));
            Assert.IsTrue(Encoders.ASCII.EncodeData(Encoders.Base32.DecodeData(base32Encoded)).Equals(text));
        }

        [TestMethod]
        public void LoremIpsum2()
        {
            const string text = "Mauris quis iaculis dui. Ut tempor mi risus, quis ornare erat vulputate vitae. Nullam a mi in lorem egestas hendrerit sit amet ut justo. Pellentesque interdum diam nec nunc feugiat, sed placerat ligula tincidunt. Aenean consequat augue odio, vitae vestibulum nisi vulputate eget. Nulla tristique condimentum neque sit amet gravida. Cras at consectetur enim. Praesent non porta tortor, pretium ultrices urna.";
            const string base32Encoded = "JVQXK4TJOMQHC5LJOMQGSYLDOVWGS4ZAMR2WSLRAKV2CA5DFNVYG64RANVUSA4TJON2XGLBAOF2WS4ZAN5ZG4YLSMUQGK4TBOQQHM5LMOB2XIYLUMUQHM2LUMFSS4ICOOVWGYYLNEBQSA3LJEBUW4IDMN5ZGK3JAMVTWK43UMFZSA2DFNZSHEZLSNF2CA43JOQQGC3LFOQQHK5BANJ2XG5DPFYQFAZLMNRSW45DFONYXKZJANFXHIZLSMR2W2IDENFQW2IDOMVRSA3TVNZRSAZTFOVTWSYLUFQQHGZLEEBYGYYLDMVZGC5BANRUWO5LMMEQHI2LOMNUWI5LOOQXCAQLFNZSWC3RAMNXW443FOF2WC5BAMF2WO5LFEBXWI2LPFQQHM2LUMFSSA5TFON2GSYTVNR2W2IDONFZWSIDWOVWHA5LUMF2GKIDFM5SXILRAJZ2WY3DBEB2HE2LTORUXC5LFEBRW63TENFWWK3TUOVWSA3TFOF2WKIDTNF2CAYLNMV2CAZ3SMF3GSZDBFYQEG4TBOMQGC5BAMNXW443FMN2GK5DVOIQGK3TJNUXCAUDSMFSXGZLOOQQG433OEBYG64TUMEQHI33SORXXELBAOBZGK5DJOVWSA5LMORZGSY3FOMQHK4TOMEXA====";
            byte[] textByteArray = Encoders.ASCII.DecodeData(text);

            Assert.IsTrue(Encoders.Base32.EncodeData(textByteArray).Equals(base32Encoded));
            Assert.IsTrue(Encoders.Base32.DecodeData(base32Encoded).SequenceEqual(textByteArray));
            Assert.IsTrue(Encoders.ASCII.EncodeData(Encoders.Base32.DecodeData(base32Encoded)).Equals(text));
        }
    }
}
