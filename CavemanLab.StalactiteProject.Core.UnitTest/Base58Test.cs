﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CavemanLab.StalactiteProject.Core.UnitTest
{
    [TestClass]
    public class Base58Test
    {
        [TestMethod]
        public void HelloWorld()
        {
            const string text = "Hello World!";
            const string base58Encoded = "2NEpo7TZRRrLZSi2U";
            byte[] textByteArray = Encoders.ASCII.DecodeData(text);

            Assert.IsTrue(Encoders.Base58.EncodeData(textByteArray).Equals(base58Encoded));
            Assert.IsTrue(Encoders.Base58.DecodeData(base58Encoded).SequenceEqual(textByteArray));
            Assert.IsTrue(Encoders.ASCII.EncodeData(Encoders.Base58.DecodeData(base58Encoded)).Equals(text));
        }

        [TestMethod]
        public void LoremIpsum1()
        {
            const string text = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam auctor mollis odio, id pharetra enim auctor nec. Suspendisse imperdiet ultricies risus, quis tempus orci faucibus in. Maecenas dapibus arcu sed lacus sagittis pulvinar. Nam ut gravida diam. Cras rutrum sed nisl quis auctor. Vestibulum facilisis ex mauris, ut volutpat urna dictum ac. Donec quis enim rhoncus, sodales libero nec, pulvinar mi. Sed scelerisque interdum dui, at pulvinar neque faucibus a. Vivamus ac sollicitudin tellus. Sed ac vestibulum eros, ac semper quam. Phasellus vulputate id risus id scelerisque. Proin imperdiet et enim id venenatis. Phasellus pulvinar eu magna sit amet tempor.";
            const string base58Encoded = "8bcYAvp46ZHpkn6gTrR1VMck8RRBCS29iKCuGSkK2sWkShaE2v8j4vtJ4aiFSDkstaHMKgWGkaDZQrerER2yUnNHvpz6owa9YbD9APp4w2TyKPATB1XXbpEZ2tPgg6KePHrz3jP3DTnFyozAfdMjhMfwaqjP2FcH4pyShipRpYaiT7L8ZsX5FgQrsU7JvvcSWP3FvqPeFuXbLNhHPR3FN9fWJKijpQestQRaMshVmEDBFrRMQhBb4C1wP8NaF5MuHoVzoxkckmJEoGeYJJrZKFVKXHRN7ZzBFZ4YgeS7samE6KtS2yWFyHVPvcCZRETVmAvkkDq8BkTxkusBvHZioRLiNAyUhraGzw6SL5FMbVUcVSBSnrJorBJra1hipFdBrPhgysHRTGAQJ5oGzCev9NxyHdF27kVhCdmzYq5g8Hu65urCmrT1ZACV2D1GpX2QQRP1ySxEEx1PjTYz3uBbV2Zhx3VLw2SH53piH8BQLHvN1wygGY4Nxrk9jtn9ufMqxJnRFtzSbXYMwuvAegLGZAvSCCEG3uHBXcgEQbAu9K123in5Pq8h8uXBtTjLx1LwBZwU4juduTUbUe1sTsPKtJ5vdbY98PzNJKLdC36EpCzAZ6KfgRuVBjWwHucg1FKXLjRfABk7JUEU4Sqir69rvqFj8VmBxuHTHcHxYgbzhkLPeebF7dgiS2UTZZCNPZHt6KXP2aFxsyD2jCfrSipR5jMGXCq32Wm4vMGdGsXx55LNPKpTQwwEK5GaKedRSuRj6d3BRy4kFGhR2DLPU7eZQgB44vSfiwMh5f5FaEgRpEsWBJuutGdZYAMngqEri2LWT9VbJVvj3ZCUYqrqq6GSyVDUkCyJUhcmbK3QXNUpiVniMcDwA9hDAaWLMWMb8mHsKjZGFwq8gXF";
            byte[] textByteArray = Encoders.ASCII.DecodeData(text);

            Assert.IsTrue(Encoders.Base58.EncodeData(textByteArray).Equals(base58Encoded));
            Assert.IsTrue(Encoders.Base58.DecodeData(base58Encoded).SequenceEqual(textByteArray));
            Assert.IsTrue(Encoders.ASCII.EncodeData(Encoders.Base58.DecodeData(base58Encoded)).Equals(text));
        }

        [TestMethod]
        public void LoremIpsum2()
        {
            const string text = "Mauris quis iaculis dui. Ut tempor mi risus, quis ornare erat vulputate vitae. Nullam a mi in lorem egestas hendrerit sit amet ut justo. Pellentesque interdum diam nec nunc feugiat, sed placerat ligula tincidunt. Aenean consequat augue odio, vitae vestibulum nisi vulputate eget. Nulla tristique condimentum neque sit amet gravida. Cras at consectetur enim. Praesent non porta tortor, pretium ultrices urna.";
            const string base58Encoded = "9YPp4KNtpbb6iLMz7WKA6hsjzBfYKXU1AK1vRfY8EtYoJdHKap9ffETiSzCC4BrsoDb5ZCicNPw6n8wFkpF5eqKSnjSBoknQxhFkZnh6shp3hPzGVzjFFW4zV3cr1eB7MmVqdYTXEX7SFSfgVNdgqBPuQpQgAkR9zvixKSZS5vbQXmizNeYK2JbKeiB1f1dGqpJfwc5rmXeGKAuHzxkRnUqtKxsAW1NdFEWFwruSFAnPdR2ov996T5CvsfKVD3gcC8dMweo6mzpy3U8VEQRy1Tf8dWoEyxJNcisfHC9XxhxRLX2x6T85epnD9bUJa5ViBmYqo7LxvJVDDL5FRJZxtWRKdq5eGBbWXBjsbSrL4acG5pjhLmWQwpidnhCs5K26xxDKuVhamU7YRCHkBS2Lm1C657GuNzNzPDwfYdZN5inw9buWpEFsTWFeorPMJyA8axxBPbCvNrxpHyEKtZg8jEehxn5HpXQHdp18oErbn4zpAQqSZNwD1uvnjrM9x9Myko4Qrbkv57JuqQv854vq55wmwLy51hD1ieB7M5f4Qd3b";
            byte[] textByteArray = Encoders.ASCII.DecodeData(text);

            Assert.IsTrue(Encoders.Base58.EncodeData(textByteArray).Equals(base58Encoded));
            Assert.IsTrue(Encoders.Base58.DecodeData(base58Encoded).SequenceEqual(textByteArray));
            Assert.IsTrue(Encoders.ASCII.EncodeData(Encoders.Base58.DecodeData(base58Encoded)).Equals(text));
        }
    }
}
