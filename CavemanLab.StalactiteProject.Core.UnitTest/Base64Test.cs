﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CavemanLab.StalactiteProject.Core.UnitTest
{
    [TestClass]
    public class Base64Test
    {
        [TestMethod]
        public void HelloWorld()
        {
            const string text = "Hello World?!";
            const string base64Encoded = "SGVsbG8gV29ybGQ/IQ==";
            byte[] textByteArray = Encoders.ASCII.DecodeData(text);

            Assert.IsTrue(Encoders.Base64.EncodeData(textByteArray).Equals(base64Encoded));
            Assert.IsTrue(Encoders.Base64.DecodeData(base64Encoded).SequenceEqual(textByteArray));
            Assert.IsTrue(Encoders.ASCII.EncodeData(Encoders.Base64.DecodeData(base64Encoded)).Equals(text));
        }
    }
}
