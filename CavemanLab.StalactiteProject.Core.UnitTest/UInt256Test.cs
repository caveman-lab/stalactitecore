using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace CavemanLab.StalactiteProject.Core.UnitTest
{
    [TestClass]
    public class UInt256Test
    {
        ulong a = 0;
        ulong b = ulong.MaxValue;
        ulong c = 15446344473569539062;
        ulong d = 2482648726375823343;
        UInt256 e = UInt256.Zero;
        UInt256 f = new UInt256(ulong.MaxValue);
        UInt256 g = new UInt256(15446344473569539062);
        UInt256 h = new UInt256(2482648726375823343);
        UInt256 i = new UInt256("0xD65C7075E16E33F6");

        [TestMethod]
        public void Operators()
        {
            Assert.AreEqual(a, e);
            Assert.AreEqual(b, f);
            Assert.AreEqual(c, g);
            Assert.AreEqual(d, h);

            Assert.IsTrue(e < f);
            Assert.IsTrue(e < g);
            Assert.IsTrue(e < h);
            Assert.IsTrue(h < g);
            Assert.IsTrue(h < f);
            Assert.IsTrue(h < c);
            Assert.IsTrue(h < b);
            Assert.IsTrue(c < f);
            Assert.IsTrue(d < f);
        }

        [TestMethod]
        public void Hex()
        {
            Assert.AreEqual(g, i);
        }

        [TestMethod]
        public void BitCount()
        {
            Assert.AreEqual(i.GetBitCount(), 64);
            Assert.AreEqual(h.GetBitCount(), 62);
        }
    }
}
